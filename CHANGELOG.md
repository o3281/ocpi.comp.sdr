# [v2.4.2](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.1...v2.4.2) (2022-05-26)

No Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

# [v2.4.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.4.0...v2.4.1) (2022-03-16)

No Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

# [v2.4.0](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.4...v2.4.0) (2022-01-25)

Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

### Enhancements
- **comp**: add new triggered sampler components. (!12)(266ef809)

### Bug Fixes
- **comp**: change "OcpiOsDebugApi.hh" to "OcpiDebugApi.hh" in triggered_sampler sources. (!16)(f370961f)
- **tests**: Fix ocpi_testing.verifier import error for register components. (!15)(a0b9d1d9)

# [v2.3.4](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.0...v2.3.1) (2021-10-13)

Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

### Bug Fixes
- **comp**: enable `prbs_synchroniser_b_c` HDL worker to take input data when its output is unconnected. (!11)(f16b2fb9)

# [v2.3.0](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

### Enhancements
- **devops**: allow CI pipelines to be launched from within project. (!9)(bca5176c)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/comp/ocpi.comp.sdr/-/compare/ab21dcec...v2.3.0-rc.1) (2021-08-25)

Initial release of contributed "Software Defined Radio Components" project for OpenCPI
