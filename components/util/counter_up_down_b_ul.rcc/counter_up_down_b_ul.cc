// Counter_up_down_b_ul implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "counter_up_down_b_ul-worker.hh"
#include <algorithm>

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Counter_up_down_b_ulWorkerTypes;

class Counter_up_down_b_ulWorker : public Counter_up_down_b_ulWorkerBase {
  bool enable = false;
  bool direction = true;
  uint32_t counter_value = 0;
  uint32_t size = 65535;
  size_t sent_samples = 0;

  RCCResult start() {
    this->sent_samples = 0;
    return RCC_OK;
  }

  RCCResult enable_written() {
    this->enable = properties().enable;
    return RCC_OK;
  }

  RCCResult direction_written() {
    this->direction = properties().direction;
    return RCC_OK;
  }

  RCCResult counter_value_written() {
    this->counter_value = properties().counter_value;
    return RCC_OK;
  }

  RCCResult size_written() {
    this->size = properties().size - 1;
    return RCC_OK;
  }

  inline void process_samples(const RCCBoolean *in_data, uint32_t *out_data,
                              size_t samples_to_process) {
    if (this->enable) {
      if (this->direction) {
        for (size_t i = 0; i < samples_to_process; i++) {
          if (*in_data++) {
            if (this->counter_value >= this->size) {
              this->counter_value = 0;
            } else {
              this->counter_value += 1;
            }
          }
          *out_data++ = this->counter_value;
        }
      } else {
        for (size_t i = 0; i < samples_to_process; i++) {
          if (*in_data++) {
            if (this->counter_value == 0 || this->counter_value > this->size) {
              this->counter_value = this->size;
            } else {
              this->counter_value -= 1;
            }
          }
          *out_data++ = this->counter_value;
        }
      }
    } else {
      // Not enabled - fill outputs with this->counter_value
      std::fill_n(out_data, samples_to_process, this->counter_value);
    }
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const RCCBoolean *inData = input.sample().data().data();
      uint32_t *outData = output.sample().data().data();

      output.setOpCode(Ulong_timed_sampleSample_OPERATION);
      size_t max_length =
          COUNTER_UP_DOWN_B_UL_OCPI_MAX_BYTES_OUTPUT / sizeof(*outData);
      inData = &inData[this->sent_samples];
      if (length - this->sent_samples <= max_length) {
        size_t unsent_samples = length - this->sent_samples;
        output.sample().data().resize(unsent_samples);
        this->process_samples(inData, outData, unsent_samples);
        // Output counter value to volatile
        properties().counter_value = this->counter_value;
        // All input data has been processed prepare for next input message
        this->sent_samples = 0;
        return RCC_ADVANCE;
      } else {
        output.sample().data().resize(max_length);
        this->process_samples(inData, outData, max_length);
        // Output counter value to volatile
        properties().counter_value = this->counter_value;
        this->sent_samples += max_length;
        output.advance(max_length);
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      /// Pass through time opcode and time data
      output.setOpCode(Ulong_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Ulong_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      this->counter_value = 0;
      // Pass through flush opcode
      output.setOpCode(Ulong_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      this->counter_value = 0;
      // Pass through discontinuity opcode
      output.setOpCode(Ulong_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Ulong_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

COUNTER_UP_DOWN_B_UL_START_INFO
COUNTER_UP_DOWN_B_UL_END_INFO
