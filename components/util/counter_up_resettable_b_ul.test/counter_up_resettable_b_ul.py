#!/usr/bin/env python3

# Python implementation of counter_up_resettable_b_ul block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import opencpi.ocpi_testing as ocpi_testing
import opencpi.ocpi_protocols as ocpi_protocols


class CounterUpResettable(ocpi_testing.Implementation):
    def __init__(self, enable, auto_reset, period, reset_count,
                 nonsample_output_select):
        super().__init__(enable=enable, auto_reset=auto_reset,
                         period=period, reset_count=reset_count,
                         nonsample_output_select=nonsample_output_select)
        self.output_ports = ["output", "outtrigger"]
        self.counter_value = 0
        self.count = self.counter_value
        self.outtrigger = False
        # Handle int rollover of period
        if self.period > 0:
            self.max_val = self.period - 1
        else:
            self.max_val = 0xFFFFFFFF

    def reset(self):
        pass

    def forward(self, opcode, data):
        message = [{"opcode": opcode, "data": data}]
        if self.nonsample_output_select is True:
            return self.output_formatter([], message)
        else:
            return self.output_formatter(message, [])

    def sample(self, values):
        output_values = [0] * len(values)
        outtrigger_values = [False] * len(values)

        # If counter enabled:
        if self.enable is True:
            # For all values in message:
            for index, value in enumerate(values):
                # If input is true, set counter to reset_count
                if value is True:
                    self.counter_value = self.reset_count
                # If input is false:
                else:
                    if self.max_val <= self.counter_value:
                        # If auto reset enabled, wrap around to 0
                        if self.outtrigger is True:
                            if self.auto_reset is True:
                                self.counter_value = 0
                    else:
                        self.counter_value = self.counter_value + 1

                # If counter has reached period:
                # or (self.period == 0):
                if self.counter_value >= self.max_val:
                    # Set outtrigger as true
                    self.outtrigger = True
                else:
                    self.outtrigger = False

                # Output counter value
                output_values[index] = self.counter_value
                outtrigger_values[index] = self.outtrigger

        # Set representation of count property
        self.count = self.counter_value

        max_message_length = ocpi_protocols.PROTOCOLS[
            "ulong_timed_sample"].max_sample_length

        # Sample messages cannot have more than max_message_length samples
        # in each message. As this component increases the data size,
        # break long messages up into messages with no more than
        # max_message_length samples.
        messages = [{
            "opcode": "sample",
            "data": output_values[index: index + max_message_length]}
            for index in range(0, len(output_values),
                               max_message_length)]
        return self.output_formatter(
            messages, [{"opcode": "sample", "data": outtrigger_values}])

    def time(self, value):
        return self.forward("time", value)

    def sample_interval(self, value):
        return self.forward("sample_interval", value)

    def discontinuity(self, value):
        self.counter_value = 0
        self.count = self.counter_value
        return self.forward("discontinuity", value)

    def flush(self, value):
        self.counter_value = 0
        self.count = self.counter_value
        return self.forward("flush", value)

    def metadata(self, value):
        return self.forward("metadata", value)
