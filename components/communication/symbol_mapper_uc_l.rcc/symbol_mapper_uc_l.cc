// RCC implementation of symbol_mapper_uc_l worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "symbol_mapper_uc_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Symbol_mapper_uc_lWorkerTypes;

class Symbol_mapper_uc_lWorker : public Symbol_mapper_uc_lWorkerBase {
  const int32_t *symbols = nullptr;
  const uint8_t mapper_length = SYMBOL_MAPPER_UC_L_LENGTH;
  size_t samples_to_send = 0;
  const uint8_t *input_value = nullptr;

  RCCResult symbols_written() {
    symbols = properties().symbols;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Uchar_timed_sampleSample_OPERATION) {
      const uint8_t *in_data = input.sample().data().data();
      int32_t *out_data = output.sample().data().data();

      const size_t max_length =
          SYMBOL_MAPPER_UC_L_OCPI_MAX_BYTES_OUTPUT / sizeof(*out_data);

      output.setOpCode(Long_timed_sampleSample_OPERATION);

      // Set number of output samples
      size_t length;
      if (samples_to_send) {
        length = samples_to_send;
      } else {
        input_value = in_data;
        length = input.sample().data().size();
        samples_to_send = length;
      }

      // Limit number of output samples
      if (length > max_length) {
        length = max_length;
      }

      output.sample().data().resize(length);

      // Load output buffer
      for (size_t i = 0; i < length; i++) {
        uint8_t symbol_key = *input_value++;
        // Limit symbol_key based on symbol mapper length
        if (symbol_key >= mapper_length) {
          symbol_key = 0;
        }
        *out_data++ = symbols[symbol_key];
      }

      // Advance data
      samples_to_send -= length;
      if (samples_to_send) {
        output.advance(length);
        return RCC_OK;
      } else {
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Uchar_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleFlush_OPERATION) {
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleDiscontinuity_OPERATION) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

SYMBOL_MAPPER_UC_L_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
SYMBOL_MAPPER_UC_L_END_INFO
