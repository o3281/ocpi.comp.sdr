#!/usr/bin/env python3

# Runs checks for Convolutional Interleaver testing.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
Verify script
"""
import struct
import numpy as np
import sys
import os.path
import conv_inter_deinterleave_utils as cid_utils

if len(sys.argv) != 3:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

dt = np.dtype("<u2")

N = int(os.environ.get("OCPI_TEST_N"))
D = int(os.environ.get("OCPI_TEST_D"))
bit_width = int(os.environ.get("OCPI_TEST_bit_width"))

num_symbols = 2**bit_width
num_zeros_pad = 0

if (num_symbols > 1):
    num_zeros_pad = D*N*(N-1)
else:
    num_zeros_pad = 0

idata = cid_utils.generate_input_data(num_symbols, num_zeros_pad)

with open(sys.argv[1], "rb") as f:
    odata = np.fromfile(f, dtype=dt)

interleave_delay_lines_regs = cid_utils.initialize_delay_lines_regs(
    N, D, "interleave")
deinterleave_delay_lines_regs = cid_utils.initialize_delay_lines_regs(
    N, D, "deinterleave")

expected_data = cid_utils.convolutional_interleave(
    N, idata, interleave_delay_lines_regs)
deinterleaved_data = cid_utils.convolutional_deinterleave(
    N, odata, deinterleave_delay_lines_regs)

idata_length = len(idata)

# Check that output data matches the input data
if np.array_equal(expected_data, odata):
    print("    Expected interleaved data and received interleaved data match")
else:
    print("    Expected interleaved data and received interleaved data do not match")
    sys.exit(1)

# Check that correct number of zeros was appended to input data, so that all
# input data makes it through the interleaver and to the output port
if np.array_equal(deinterleaved_data[num_zeros_pad:], idata[0:idata_length-num_zeros_pad]):
    print("    Expected deinterleaved data and input data match")
else:
    print("    Expected deinterleaved data and input data do not match")
    sys.exit(1)
