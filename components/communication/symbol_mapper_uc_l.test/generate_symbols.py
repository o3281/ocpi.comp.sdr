#!/usr/bin/env python3

# Generates the symbols for symbol_mapper_uc_l testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys
import random
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description="Writes symbols to a file")
    parser.add_argument("-s", "--seed", type=int, default=None,
                        help="Sets the seed for the random number generator")
    parser.add_argument("file_path", type=str,
                        help="File path to save generated symbols")
    arguments = parser.parse_args()
    return arguments


def generate_symbols():
    # Parse arguments
    args = parse_args()
    # Set seed
    if args.seed is not None:
        random.seed(args.seed)
    else:
        random.seed(0)
    # Set symbol mapper length
    mapper_length = int(os.environ.get("OCPI_TEST_length"))
    # Generate random symbols
    symbols = random.sample(range(-2147483648, 2147483647), mapper_length)
    # Open file
    symbol_file = open(args.file_path, "w")
    # Write symbols to file
    for i in symbols:
        symbol_file.write("".join(str(i) + "\n"))
    # Close file
    symbol_file.close()


def main():
    generate_symbols()


if __name__ == "__main__":
    main()
