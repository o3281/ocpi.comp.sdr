.. convolutional_interleaver_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _convolutional_interleaver_us:


Convolutional Interleaver (``convolutional_interleaver_us``)
============================================================
Takes in symbols that are of the data type, unsigned short, in a burst transmission and rearranges the order of the symbols.

Tested platforms include ``xsim``, ``e31x``.

Design
------
The convolutional interleaver component rearranges the input symbols in a burst transmission. It is used to reduce and spread out burst errors.

The convolutional interleaver is constructed by having N different delay lines with each line having a greater delay than the previous line. Each new input symbol
is placed on a different delay line. The first delay line has 0 delay. The number of delay lines is defined as N and the depth of the delay line shift registers is
defined as D. N should be set to a value greater than 1.

To ensure that all the input symbols make it through the convolutional interleaver, the data burst should be padded with D*N*(N-1) extra symbols.

A block diagram representation of the implementation is given in :numref:`convolutional_interleaver_us-diagram`.

.. _convolutional_interleaver_us-diagram:

.. figure:: convolutional_interleaver_us.svg
   :alt: Block diagram outlining convolutional interleaver implementation.
   :align: center

   Block diagram outlining convolutional interleaver implementation.

Interface
---------
.. literalinclude:: ../specs/convolutional_interleaver_us-spec.xml
   :language: xml

Ports
~~~~~
.. ocpi_documentation_ports::

   input:  Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The convolutional interleaver interleaves data with sample opcodes only.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../convolutional_interleaver_us.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependencies for this worker are:

 * ``shift_reg.vhd``

 * ``convolutional_delay_lines.vhd``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ocpi.util.all;``

Limitations
-----------
Limitations of ``convolutional_interleaver_us`` are:

 * None.

Testing
-------
This component test suite uses the OpenCPI unit test framework. The unit test sends :math:`2^{bit\_width}` symbols and with some zeros padding the symbols, so that all the input symbols are output by the convolutional interleaver. It is then verified to make sure that the symbols were interleaved correctly and that all the symbols made it through the convolutional interleaver.

.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
