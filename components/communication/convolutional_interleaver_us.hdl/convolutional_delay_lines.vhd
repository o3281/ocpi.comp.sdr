-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- Convolutional Delay Lines
-------------------------------------------------------------------------------
--
-- Description:
--
-- Implements the delay lines for the convolutional interleaver/dei-interleaver.
-- The zero delay line is not output by the convolutional_delay_lines; it only
-- outputs delay lines with delays greater than 0.

entity convolutional_delay_lines is
    generic (
      N         : natural   := 12;
      WIDTH     : natural   := 8;
      D         : natural   := 17;
      MODE      : std_logic := '0');
    port (
      clk       : in  std_logic;
      rst       : in  std_logic;
      en        : in  std_logic_vector(N-2 downto 0);
      din       : in  std_logic_vector(WIDTH-1 downto 0);
      dout      : out std_logic_vector(((N-1)*WIDTH)-1 downto 0));
end entity convolutional_delay_lines;

architecture rtl of convolutional_delay_lines is

begin
  
  interleave : if MODE = '0' generate
    delay_lines : for i in 0 to N-2 generate
      shift_regs : entity work.shift_reg
        generic map(WIDTH   => WIDTH,
                    DEPTH   => D*(i+1))
        port map   (clk     => clk,
                    en      => en(i),
                    din     => din,
                    dout    => dout((WIDTH*(i+1))-1 downto WIDTH*i));
    end generate;
  end generate;
 
  deinterleave : if MODE = '1' generate
    delay_lines : for i in 0 to N-2 generate
      shift_regs : entity work.shift_reg
        generic map(WIDTH   => WIDTH,
                    DEPTH   => D*(N-i-1))
        port map   (clk     => clk,
                    en      => en(i),
                    din     => din,
                    dout    => dout((WIDTH*(i+1))-1 downto WIDTH*i));
    end generate;
  end generate;




end rtl;
