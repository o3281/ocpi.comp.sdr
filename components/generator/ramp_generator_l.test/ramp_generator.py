#!/usr/bin/env python3

# Python implementation of ramp generator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import math
import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class RampGenerator(ocpi_testing.Implementation):
    def __init__(self, enable, message_length,
                 step_size,
                 start_amplitude, stop_amplitude,
                 discontinuity_on_amplitude_change,
                 discontinuity_on_step_size_change):

        # The below setting of the discontinuity_on_amplitude_change line is
        # one character longer than the 80 character line limit. As breaking
        # the line does not improve readability and automatic formatters return
        # to a single line, this is left too long in this case.
        super().__init__(
            enable=enable, message_length=message_length,
            step_size=step_size,
            start_amplitude=start_amplitude, stop_amplitude=stop_amplitude,
            discontinuity_on_amplitude_change=discontinuity_on_amplitude_change,
            discontinuity_on_step_size_change=discontinuity_on_step_size_change)

        self.start_amplitude = np.int32(self.start_amplitude)
        self.stop_amplitude = np.int32(self.stop_amplitude)
        self.step_size = np.int32(self.step_size)

        self.count = self.start_amplitude

        self.input_ports = []

    def reset(self):
        self.count = self.start_amplitude

    def sample(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def time(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def sample_interval(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def flush(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def discontinuity(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def sample_interval(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def platform_metadata(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def application(self, *args):
        TypeError("Ramp generator does not have any input ports")

    def generate(self, total_output_length):

        if self.enable:

            output = np.zeros(total_output_length, dtype=np.int32)

            # Set output values
            for sample in range(total_output_length):
                output[sample] = self.count
                next_count = np.int32(self.count + self.step_size)
                if self.step_size >= 0:
                    if self.count >= self.stop_amplitude:
                        self.count = self.start_amplitude
                    else:
                        self.count = next_count
                else:
                    if self.count <= self.stop_amplitude:
                        self.count = self.start_amplitude
                    else:
                        self.count = next_count

            # Split output data into message sizes
            number_of_messages = math.ceil(len(output) / self.message_length)
            message_data = [[]] * number_of_messages
            for index in range(number_of_messages):
                message_data[index] = output[index * self.message_length:
                                             (index + 1) * self.message_length]

            return self.output_formatter([
                {"opcode": "sample", "data": data} for data in message_data])

        else:
            return self.output_formatter([])
