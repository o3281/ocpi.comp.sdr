-- HDL Implementation of a square wave generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  -- interface constants
  constant delay_c        : integer := 1;
  constant opcode_width_c : integer :=
    integer(ceil(log2(real(long_timed_sample_opcode_t'pos(long_timed_sample_opcode_t'high)+1))));

  function opcode_to_slv(inop : in long_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(long_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return long_timed_sample_opcode_t is
  begin
    return long_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  -- Interface signals
  signal enable      : std_logic;
  signal output_hold : std_logic;

  signal amplitude_change_discontinuity : std_logic;
  signal cycle_change_discontinuity     : std_logic;

  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data   : std_logic_vector(output_out.data'length - 1 downto 0);

  signal square_out : std_logic_vector(output_out.data'length - 1 downto 0);

  -- State machine signals
  type state_t is (on_s, off_s);
  signal current_state : state_t;
  signal count         : unsigned(props_in.on_samples'length - 1 downto 0);

begin

  -- Enable the generator when enable property is true and output is ready,
  -- but not when the interface module is outputting other message signals.
  enable <= output_in.ready and props_in.enable and not output_hold;

  amplitude_change_discontinuity <= '1' when ((props_in.on_amplitude_written = '1' or
                                               props_in.off_amplitude_written = '1') and
                                              props_in.discontinuity_on_amplitude_change = '1')
                                    else '0';

  cycle_change_discontinuity <= '1' when ((props_in.on_samples_written = '1' or
                                           props_in.off_samples_written = '1') and
                                          props_in.discontinuity_on_cycle_change = '1')
                                else '0';

  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      processed_data_opcode_g => opcode_to_slv(long_timed_sample_sample_op_e),
      discontinuity_opcode_g  => opcode_to_slv(long_timed_sample_discontinuity_op_e)
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      discontinuity_trigger => amplitude_change_discontinuity,
      generator_enable      => props_in.enable,
      generator_reset       => cycle_change_discontinuity,
      processed_stream_in   => square_out,
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

  square_wave_generator_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1' or cycle_change_discontinuity = '1') then
        current_state <= on_s;
        count         <= (others => '0');
      elsif enable = '1' then
        case current_state is
          when on_s =>
            if count = (props_in.on_samples - 1) then
              current_state <= off_s;
              count         <= (others => '0');
            else
              count <= count + 1;
            end if;
            square_out <= std_logic_vector(props_in.on_amplitude);
          when off_s =>
            if count = (props_in.off_samples - 1) then
              current_state <= on_s;
              count         <= (others => '0');
            else
              count <= count + 1;
            end if;
            square_out <= std_logic_vector(props_in.off_amplitude);
        end case;
      end if;
    end if;
  end process;
end rtl;
