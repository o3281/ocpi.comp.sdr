#!/usr/bin/env python3

# Python implementation of splitter block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import opencpi.ocpi_testing as ocpi_testing


class Splitter(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()
        self.output_ports = ["output_1", "output_2", "output_3", "output_4"]

    def reset(self):
        pass

    def splitter(self, opcode, data):
        return self.output_formatter(
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}],
            [{"opcode": opcode, "data": data}])

    def sample(self, values):
        return self.splitter("sample", values)

    def time(self, value):
        return self.splitter("time", value)

    def sample_interval(self, value):
        return self.splitter("sample_interval", value)

    def discontinuity(self, indicator):
        if indicator is True:
            return self.splitter("discontinuity", None)
        else:
            return self.output_formatter([], [], [], [])

    def flush(self, indicator):
        if indicator is True:
            return self.splitter("flush", None)
        else:
            return self.output_formatter([], [], [], [])

    def metadata(self, value):
        return self.splitter("metadata", value)
