#!/usr/bin/env python3

# Runs checks for register_l testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys

import opencpi.ocpi_testing as ocpi_testing
from opencpi.ocpi_testing.ocpi_testing.verifier import BaseComparison


class InSetComparison(BaseComparison):
    def variable_summary(self):
        return {}

    def same(self, reference, implementation):
        """ Checks that the dataset is the right opcode """
        for ref_msg_index, reference_message in enumerate(reference):
            if reference_message["opcode"] != "sample":
                continue
            for index, implementation_message in enumerate(implementation):
                if implementation_message["opcode"] != "sample":
                    return False, (f"Message {index} (zero indexed) is of an"
                                   + " unexpected opcode")
                # We cannot check the actual data, as the position within
                # the stream is unknown.
        # Checks pass, no failure message
        return True, ""


class Register(ocpi_testing.Implementation):
    def reset(self):
        pass

    def sample(self, values):
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

register_implementation = Register()

test_id = ocpi_testing.get_test_case()

# Use a custom verifier, which will just check the opcodes.
ocpi_testing.ocpi_testing.verifier.COMPARISON_METHODS["set"] = InSetComparison
verifier = ocpi_testing.Verifier(register_implementation)
verifier.set_port_types(
    ["long_timed_sample"], ["long_timed_sample"], ["set"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
