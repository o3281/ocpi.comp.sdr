#!/usr/bin/env python3

# Python implementation of multiplexer block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import opencpi.ocpi_testing as ocpi_testing


class Multiplexer(ocpi_testing.Implementation):
    def __init__(self, port_select):
        super().__init__(port_select=port_select)

        self.input_ports = ["input_1", "input_2"]

    def reset(self):
        pass

    def select_input(self, input_1, input_2):
        if not self.port_select:
            return 0
        else:
            return 1

    def sample(self, input_1, input_2):
        if not self.port_select:
            output_values = input_1
        else:
            output_values = input_2

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
