#!/usr/bin/env python3

# Runs checks for Golay Encoder testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
Verify script
"""
import struct
import numpy as np
import sys
import os.path
import golay_utils

if len(sys.argv) != 3:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

dt = np.dtype("<u4")

checkbits_mask = int(os.environ.get("OCPI_TEST_checkbits_mask"))

# Open input file and grab samples as uint32
with open(sys.argv[1], "rb") as f:
    odata = np.fromfile(f, dtype=dt)

# Apply mask to codeword to undo toggling of checkbits done by worker
for i, data in enumerate(odata):
    odata[i] = data ^ (checkbits_mask << 12)

expected_codewords = golay_utils.gen_codewords()

# Check that output data matches the input data
if np.array_equal(expected_codewords, odata):
    print("    Expected Golay codewords and received Golay codewords match")
else:
    print("    Expected Golay codewords and received Golay codewords do not match")
    sys.exit(1)
