#!/usr/bin/env python3

# Runs checks for Golay Decoder testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
Verify script

Verify args:
1. Test case number
2. Output data file used for validation
3. Input data file used for comparison

"""
import struct
import numpy as np
import sys
import os.path
import golay_utils

if len(sys.argv) != 4:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

dt = np.dtype("<u2")

simulation = os.environ.get("OCPI_TEST_simulation")

test_case = int(sys.argv[1])
num_codewords = 4096
num_error_patterns = 2048

with open(sys.argv[2], "rb") as f:
    odata = np.fromfile(f, dtype=dt)


if test_case == 1:
    idata = np.empty(num_codewords, dtype=dt)
    for i in range(0, num_codewords):
        idata[i] = i

    # Check that output data matches the input data
    if np.array_equal(idata, odata):
        print("    Expected decoded data and decoded data match")
    else:
        print("    Expected decoded data and decoded data do not match")
        sys.exit(1)
elif test_case == 2:
    if simulation == "true":
        idata_length = num_error_patterns*5
    else:
        idata_length = num_error_patterns*num_codewords

    idata = np.empty(idata_length, dtype=dt)
    j = 0
    for i in range(0, idata_length):
        if (i > 0 and i % num_error_patterns == 0):
            j += 1
        idata[i] = j

    # Check that output data matches the input data
    if np.array_equal(idata, odata):
        print("    Expected decoded data and decoded data match")
    else:
        print("    Expected decoded data and decoded data do not match")
        sys.exit(1)
else:
    print("Invalid test scenario: valid test scenarios are 1 and 2")
    sys.exit(1)
