// CIC Filter Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_
#define COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>

// LINT EXCEPTION: cpp_011: 3: New class definition allowed in commmon
// implementation
template <class TYPE>
class cic_core {
 public:
  cic_core(uint8_t cic_order, uint8_t cic_diff_delay) {
    set_cic_parameters(cic_order, cic_diff_delay);
  }

  ~cic_core() {
    if (m_delay_buffer != NULL) delete m_delay_buffer;
    if (m_previous_input != NULL) delete m_previous_input;
  }

  // Reset the member's variables and buffers
  void reset() {
    // Configure delay buffer
    if (m_delay_buffer != NULL) delete m_delay_buffer;
    m_delay_buffer = new complex_longlong[m_delay_buffer_size];

    m_delay_buffer_read = m_delay_buffer;
    m_delay_buffer_write = m_delay_buffer;
    m_delay_buffer_end = m_delay_buffer + m_delay_buffer_size;

    // Fill delay buffer with enough blank values to add zero comb filtering to
    // the first inputs
    for (uint8_t i = 0; i < m_cic_diff_delay; i++) {
      for (uint8_t ii = 0; ii < m_cic_order; ii++) {
        m_delay_buffer_write->real = 0;
        m_delay_buffer_write->imaginary = 0;

        m_delay_buffer_write++;
        if (m_delay_buffer_write >= m_delay_buffer_end) {
          m_delay_buffer_write = m_delay_buffer;
        }
      }
    }

    // Configure previous input buffers
    if (m_previous_input != NULL) delete m_previous_input;
    m_previous_input = new complex_longlong[m_previous_input_size];

    for (uint8_t i = 0; i < m_previous_input_size; i++) {
      m_previous_input[i].real = 0;
      m_previous_input[i].imaginary = 0;
    }

    m_sample_count = m_down_sample_factor;
  }

  // Set the cic parameters and reset to create correct length memory
  void set_cic_parameters(uint8_t cic_order, uint8_t cic_diff_delay) {
    m_cic_order = cic_order;
    m_cic_diff_delay = cic_diff_delay;

    // Maximum number of delayed inputs to store is given by multiplying the
    // delay factor by the number of cic stages. An additional 1 is added to the
    // delay buffer size to account for the newest value written to the end of
    // the buffer before the oldest value is read and circular overwriting can
    // occur
    m_delay_buffer_size = (cic_order * cic_diff_delay) + 1;

    // Maximum number of previous inputs is given by the cic_order size
    m_previous_input_size = cic_order;

    reset();
  }

  void set_down_sample_factor(uint16_t down_sample_factor) {
    m_down_sample_factor = down_sample_factor;

    // Starts equal to down sample factor so first sample is counted
    m_sample_count = m_down_sample_factor;
  }

  void set_scale_output(uint16_t scale_output) {
    m_scale_output = scale_output;
  }

  // Do cic decimator work. Returns the length of output data.
  size_t do_decimator_work(const TYPE input[], size_t samples, TYPE output[]) {
    size_t output_length = 0;

    for (uint16_t i = 0; i < samples; i++) {
      complex_longlong modified_input = {input[i].real, input[i].imaginary};
      // Integrate stage
      modified_input = integrate_input(modified_input);

      // Downsampling stage
      m_sample_count++;
      if (m_sample_count >= m_down_sample_factor) {
        // Comb filter stage
        modified_input = comb_filter_input(modified_input);

        // Scaling stage
        modified_input = scale_input(modified_input);

        output[output_length].real = modified_input.real;
        output[output_length].imaginary = modified_input.imaginary;
        output_length++;
        m_sample_count = 0;
      }
    }

    return output_length;
  }

 private:
  struct complex_longlong {
    int64_t real;
    int64_t imaginary;
  };

  complex_longlong *m_delay_buffer = NULL;
  uint16_t m_delay_buffer_size;
  complex_longlong *m_delay_buffer_read;
  complex_longlong *m_delay_buffer_write;
  complex_longlong *m_delay_buffer_end;

  complex_longlong *m_previous_input = NULL;
  uint8_t m_previous_input_size;

  uint8_t m_cic_order;
  uint8_t m_cic_diff_delay;
  uint16_t m_down_sample_factor = 0;
  uint8_t m_scale_output = 0;

  uint16_t m_sample_count;

  // Applies an integrator to a given input and returns the modified input.
  complex_longlong integrate_input(complex_longlong input) {
    complex_longlong modified_input = {input.real, input.imaginary};

    for (uint8_t stage = 0; stage < m_cic_order; stage++) {
      modified_input.real = m_previous_input[stage].real + modified_input.real;
      modified_input.imaginary =
          m_previous_input[stage].imaginary + modified_input.imaginary;

      m_previous_input[stage].real = modified_input.real;
      m_previous_input[stage].imaginary = modified_input.imaginary;
    }

    return modified_input;
  }

  // Applies a comb filter to a given input and returns the modified input
  complex_longlong comb_filter_input(complex_longlong input) {
    complex_longlong modified_input = {input.real, input.imaginary};

    for (uint8_t stage = 0; stage < m_cic_order; stage++) {
      m_delay_buffer_write->real = modified_input.real;
      m_delay_buffer_write->imaginary = modified_input.imaginary;

      m_delay_buffer_write++;
      if (m_delay_buffer_write >= m_delay_buffer_end) {
        m_delay_buffer_write = m_delay_buffer;
      }

      modified_input.real -= m_delay_buffer_read->real;
      modified_input.imaginary -= m_delay_buffer_read->imaginary;

      m_delay_buffer_read++;
      if (m_delay_buffer_read >= m_delay_buffer_end) {
        m_delay_buffer_read = m_delay_buffer;
      }
    }

    return modified_input;
  }

  // Scales a given input by the class member's scale factor and returns the
  // scaled input
  complex_longlong scale_input(complex_longlong input) const {
    complex_longlong modified_input = {input.real, input.imaginary};

    if (m_scale_output > 0) {
      // Scale output with half up rounding
      // Half the scale output value is added to the input to ensure
      // integer truncation results in half up rounding
      modified_input.real =
          (modified_input.real + (1 << (m_scale_output - 1))) >> m_scale_output;
      modified_input.imaginary =
          (modified_input.imaginary + (1 << (m_scale_output - 1))) >>
          m_scale_output;
    }
    return modified_input;
  }
};

#endif  // COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_
