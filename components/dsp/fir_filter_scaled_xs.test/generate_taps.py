#!/usr/bin/env python3

# Generates taps for fir_filter_scaled_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import argparse


def csv_split(string):
    return string.split(",")


def parse_args():
    parser = argparse.ArgumentParser(description="Writes fir taps to a file")
    parser.add_argument("-t", "--taps", type=csv_split, required=True,
                        help="sets the taps explicitly, e.g., " +
                        "\"--taps=-1,0,1,2,...\"")
    parser.add_argument("file_path", type=str,
                        help="File path to save generated taps")
    arguments = parser.parse_args()
    return arguments.taps, arguments.file_path


def taps_to_file(taps, file_path):
    taps_file = open(file_path, "w")
    for val in taps:
        taps_file.write(val + "\n")
    taps_file.close()


def main():
    taps, file_path = parse_args()
    taps_to_file(taps, file_path)


if __name__ == "__main__":
    main()
