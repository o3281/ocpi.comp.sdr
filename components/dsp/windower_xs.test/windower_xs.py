#!/usr/bin/env python3

# Python implementation of windower_xs
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import opencpi.ocpi_testing as ocpi_testing
import numpy


class Windower(ocpi_testing.Implementation):
    def __init__(self, window_length, coefficients):
        super().__init__()

        self._window_length = window_length
        # Assume the window_length takes priority over the actual length of the
        # coefficient list
        self._coefficients = coefficients[:window_length]
        if window_length > len(self._coefficients):
            self._coefficients += [0] * \
                (window_length - len(self._coefficients))
        # Initialise internal state
        self.coefficient_index = 0

    def reset(self):
        self.coefficient_index = 0

    def sample(self, input_):
        output = list(input_)
        output = [self._calculate_output(index) for index in input_]
        return self.output_formatter([
            {"opcode": "sample", "data": output}])

    def flush(self, input_):
        self.coefficient_index = 0
        return self.output_formatter([
            {"opcode": "flush", "data": input_}])

    def discontinuity(self, input_):
        self.coefficient_index = 0
        return self.output_formatter([
            {"opcode": "discontinuity", "data": input_}])

    def _calculate_output(self, input_xs):
        # Apply coefficient to input value
        coefficient = self._coefficients[self.coefficient_index]
        real = numpy.short(round((input_xs.real * coefficient) / 2**32))
        imag = numpy.short(round((input_xs.imag * coefficient) / 2**32))
        result = complex(real, imag)
        # Increment coefficient index
        self.coefficient_index = (
            self.coefficient_index + 1) % self._window_length
        # Done
        return result
