// RCC implementation of cic_decimator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../common/cic/cic_core.hh"
#include "cic_decimator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Cic_decimator_xsWorkerTypes;

class Cic_decimator_xsWorker : public Cic_decimator_xsWorkerBase {
  uint8_t cic_order = CIC_DECIMATOR_XS_CIC_ORDER;
  uint8_t cic_diff_delay = CIC_DECIMATOR_XS_CIC_DIFF_DELAY;

  uint16_t down_sample_factor = 2;
  uint8_t scale_output = 0;
  uint16_t flush_length = 0;

  cic_core<Complex_short_timed_sampleSampleData> *cic = NULL;

  // Delayed timestamp handling
  bool timestamp_received = false;
  uint64_t current_sample_time_fraction = 0;
  uint64_t current_sample_time_seconds = 0;
  uint64_t sample_interval_fraction = 0;
  uint64_t sample_interval_seconds = 0;
  uint16_t sample_count = 0;

  // Tracks how much data has been flushed when flush_length is too long to be
  // contained within a single message
  uint16_t flushed_data_length = 0;

  RCCResult start() {
    if ((this->cic_diff_delay != 1) && (this->cic_diff_delay != 2)) {
      setError("cic diff delay must be set to 1 or 2");
      return RCC_FATAL;
    }

    if (this->cic == NULL) {
      this->cic = new cic_core<Complex_short_timed_sampleSampleData>(
          this->cic_order, this->cic_diff_delay);
    }

    // Sample count starts equal to down sample factor to ensure that the first
    // sample is counted
    this->sample_count = down_sample_factor;
    return RCC_OK;
  }

  RCCResult stop() {
    delete this->cic;
    return RCC_OK;
  }

  // Notification that down_sample_factor property has been written
  RCCResult down_sample_factor_written() {
    if (properties().down_sample_factor == 0) {
      setError("down sample factor must be greater than 0");
      return RCC_FATAL;
    }
    if (this->cic == NULL) {
      this->cic = new cic_core<Complex_short_timed_sampleSampleData>(
          this->cic_order, this->cic_diff_delay);
    }
    this->cic->set_down_sample_factor(properties().down_sample_factor);
    this->down_sample_factor = properties().down_sample_factor;

    // Sample count starts equal to down sample factor to ensure that the first
    // sample is counted
    this->sample_count = this->down_sample_factor;
    return RCC_OK;
  }

  // Notification that scale_output property has been written
  RCCResult scale_output_written() {
    if (this->cic == NULL) {
      this->cic = new cic_core<Complex_short_timed_sampleSampleData>(
          this->cic_order, this->cic_diff_delay);
    }

    this->cic->set_scale_output(properties().scale_output);
    return RCC_OK;
  }

  // Notification that flush_length property has been written
  RCCResult flush_length_written() {
    this->flush_length = properties().flush_length;
    return RCC_OK;
  }

  // Increments the stored timestamp by the stored sample rate
  void increment_timestamp() {
    // Check for fractional overflow
    if (this->current_sample_time_fraction >
        (UINT64_MAX - this->sample_interval_fraction)) {
      // Calculate the remaining fraction after the overflow
      this->current_sample_time_fraction =
          this->sample_interval_fraction -
          (UINT64_MAX - this->current_sample_time_fraction);
      this->current_sample_time_seconds++;
    } else {
      this->current_sample_time_fraction += this->sample_interval_fraction;
    }
    this->current_sample_time_seconds += this->sample_interval_seconds;
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t input_length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *inputData =
          input.sample().data().data();
      Complex_short_timed_sampleSampleData *outputData;
      uint16_t output_length = 0;

      if (!this->timestamp_received) {
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        outputData = output.sample().data().data();
      }

      // Pass through zero length messages
      if (input_length == 0) {
        output.sample().data().resize(0);
        return RCC_ADVANCE;
      }

      // If there is a pending timestamp then data will not be sent until
      // timestamp is passed
      if (this->timestamp_received) {
        // Get local copy of the counter. We can't act directly on the counter
        // as when a timestamp needs to be sent we just step through the data
        // and the counter is not updated
        uint16_t down_sample_count = this->sample_count;
        for (size_t i = 0; i < input_length; i++) {
          down_sample_count++;
          if (down_sample_count >= this->down_sample_factor) {
            this->timestamp_received = false;
            output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
            output.time().fraction() = this->current_sample_time_fraction;
            output.time().seconds() = this->current_sample_time_seconds;
            output.advance();
            return RCC_OK;
          } else {
            this->increment_timestamp();
          }
        }
      }

      // Iterate through input samples to calculate number of samples that will
      // remain after output processing. This count is needed for timestamp
      // handling
      for (size_t i = 0; i < input_length; i++) {
        this->sample_count++;
        if (this->sample_count >= this->down_sample_factor) {
          this->sample_count = 0;
        }
      }

      // In the case where there was not enough data within input to reach the
      // downsample rate, cic work can still commence in order to process and
      // save the previous input, but no output will be generated
      output_length =
          this->cic->do_decimator_work(inputData, input_length, outputData);

      // If downsample rate was reached, meaning data was processed
      if (output_length > 0) {
        // Pass through processed output data
        output.sample().data().resize(output_length);
        return RCC_ADVANCE;
      } else {
        // If not enough input data was present to produce an output
        // Get more input data
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Save time data. Opcode is not passed through until an
      // output stream value is generated
      this->current_sample_time_fraction = input.time().fraction();
      this->current_sample_time_seconds = input.time().seconds();
      this->timestamp_received = true;
      input.advance();
      return RCC_OK;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Save the sample interval value for potential timestamp adjustment
      this->sample_interval_fraction = input.sample_interval().fraction();
      this->sample_interval_seconds = input.sample_interval().seconds();
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (this->flushed_data_length < this->flush_length) {
        // When a flush is requested input the set number of zeros into the cic
        // worker
        Complex_short_timed_sampleSampleData zeroed_input
            [CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
             sizeof(Complex_short_timed_sampleSampleData)] = {{0, 0}};
        uint16_t input_length = 0;
        Complex_short_timed_sampleSampleData *outputData =
            output.sample().data().data();
        uint16_t output_length = 0;

        // Cast the result of the unsigned integer subtraction to uint16_t to
        // prevent integral promotion.
        if (static_cast<uint16_t>(this->flush_length -
                                  this->flushed_data_length) <=
            CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
                sizeof(Complex_short_timed_sampleSampleData)) {
          input_length = this->flush_length - this->flushed_data_length;
        } else {
          // Output maximum amount of data that will fit into a single message
          // (ocpi.comp.sdr limit for samples in message is given by
          // CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
          // sizeof(Complex_short_timed_sampleSampleData))
          input_length = CIC_DECIMATOR_XS_OCPI_MAX_BYTES_OUTPUT /
                         sizeof(Complex_short_timed_sampleSampleData);
        }

        // Iterate through input samples to calculate number of samples that
        // will remain after output processing. This count is needed for
        // timestamp handling.
        for (size_t i = 0; i < input_length; i++) {
          this->sample_count++;
          if (this->sample_count >= this->down_sample_factor) {
            this->sample_count = 0;
          }
        }

        // In the case where there was not enough data within input to reach the
        // downsample rate, cic work can still commence in order to process and
        // save the previous input, but no output will be generated
        output_length = this->cic->do_decimator_work(zeroed_input, input_length,
                                                     outputData);

        this->flushed_data_length += input_length;
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        output.sample().data().resize(output_length);
        output.advance();
        return RCC_OK;
      } else {
        // Pass through flush opcode
        output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
        this->flushed_data_length = 0;
        return RCC_ADVANCE;
      }
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

CIC_DECIMATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CIC_DECIMATOR_XS_END_INFO
