// RCC implementation of frequency_demodulator_xs_s worker
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "frequency_demodulator_xs_s-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Frequency_demodulator_xs_sWorkerTypes;

class Frequency_demodulator_xs_sWorker
    : public Frequency_demodulator_xs_sWorkerBase {
  Complex_short_timed_sampleSampleData lastSample;

  RCCResult start() {
    this->lastSample.real = 0;
    this->lastSample.imaginary = 0;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *inData =
          input.sample().data().data();
      int16_t *outData = output.sample().data().data();

      output.setOpCode(Short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      for (size_t i = 0; i < length; i++) {
        // conj(lastSample) x *inData = (ac - -bd) + i(ad + -bc)
        // a = real(lastSample)
        // b = imag(lastSample)
        // c = real(*inData)
        // d = imag(*inData)

        int64_t diff_real =
            static_cast<int64_t>(this->lastSample.real) * inData->real +
            static_cast<int64_t>(this->lastSample.imaginary) *
                inData->imaginary;
        int64_t diff_imag =
            static_cast<int64_t>(this->lastSample.real) * inData->imaginary -
            static_cast<int64_t>(this->lastSample.imaginary) * inData->real;

        *outData++ = atan2(diff_imag, diff_real) / M_PI * 32767 + 0.5;

        this->lastSample = *inData++;
      }

      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

FREQUENCY_DEMODULATOR_XS_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FREQUENCY_DEMODULATOR_XS_S_END_INFO
