.. windower_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _windower_xs-HDL-worker:


``windower_xs`` HDL Worker
==========================

Detail
------
.. ocpi_documentation_worker::

All data that is not sample data is sent through the protocol interface delay
block and a pipeline delay is added. Any sample data is stripped off and sent
into the window function primitives.

The diagram below shows for complex data there will be two window primitives
instantiated. At start-up the coefficient DPRAM is filled with coefficient data
from the raw property interface. When a sample comes into this component the
coefficient counter is incremented which is used as the DPRAM address to read
out the next coefficient. The data out from the window primitives is multiplexed
back into the protocol within the protocol interface delay. This component is
designed so that the coefficients can be changed during runtime.

.. _windower_xs-hdl-diagram:

.. figure:: ../windower_xs.comp/window_block_diagram.svg
   :alt: Block diagram outlining windower implementation.
   :align: center

   Windower implementation.

Utilisation
-----------
.. ocpi_documentation_utilization::
