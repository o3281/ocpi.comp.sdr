#!/usr/bin/env python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
This script is used to generate the 4096 possible
Golay codewords for a Golay [24,12,8] codeword.

It also generates the 2048 possible error patterns for 3 bit errors and 
generates the Golay syndrome. 

The encoding used in this script is based off of this article:
http://aqdi.com/articles/using-the-golay-error-detection-and-correction-code-3/

The syndrome generation used by the script uses these references:
https://www.researchgate.net/profile/Satyabrata-Sarangi/publication/264977426_Efficient_Hardware_Implementation_of_Encoder_and_Decoder_for_Golay_Code/links/5405fdd00cf23d9765a7cbca/Efficient-Hardware-Implementation-of-Encoder-and-Decoder-for-Golay-Code.pdf
https://www.sciencedirect.com/science/article/pii/S1665642313715438
https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.323.9943&rep=rep1&type=pdf
http://www.rajivchakravorty.com/source-code/uncertainty/multimedia-sim/html/golay_8c-source.html
"""

import numpy as np

characteristic_polynomial = 0xAE3
num_codewords = 4096
# Golay [24,12,8] can correct up to 3 errors
max_num_errors = 3
# 2048 errors that are less than or equal to 3
num_error_patterns = 2048

# Reverses the bits in a number
def reverse_bits(data, num_bits):
    width = '0' + str(num_bits) + 'b' 
    return int(format(data, width)[::-1], 2)

# Referenced the accepted answer: 
# https://stackoverflow.com/questions/17027878/algorithm-to-find-the-most-significant-bit
# Finds the MSB one bit in a number
def find_msb_one(data):
    count = 0
    temp = data
    if(data > 0):
        while temp > 0:
            temp = temp >> 1
            count += 1
    return count

# Taken from https://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
# Generates a parity bit
def parity(data):
    data ^= data >> 16
    data ^= data >> 8
    data ^= data >> 4
    data &= 0xf
    return (0x6996 >> data) & 1

# Uses mod 2 division to generate Golay codeword
def golay_encode(data):
    checkbits = 0      
    index = find_msb_one(data)
    result = 0
    # If no binary one bits in the number, the  checkbit is set to 0
    if (index == 0):
        checkbits = 0
    else:
        # Have to shift over MSB one by 12-index so
        # that it is shifted to the 12th bit
        result = data << 12-index
        result = result^characteristic_polynomial
        count = 11-(12-index)
        # Have 11 zeros to append ot the original data
        # Instead of appending zeros just shift over until 
        # 11 shifts happen.
        while count > 0:
            index = find_msb_one(result)
            # If can't shift the MSB one bit until 
            # it is the 12th bit, shift by remaining
            # number of zeros and break
            if (12-index > count):
                result = result << count
                break
            result = result << 12-index
            result = result^characteristic_polynomial
            count -= 12-index
    checkbits = result
    codeword = (checkbits << 12) + data
    parity_bit = parity(codeword)
    codeword += (parity_bit << 23)
    return codeword

# Generates at the 4096 possible codewords
def gen_codewords():
    codewords = np.empty(num_codewords, dtype=np.uint32)
    for i in range(0, num_codewords):
        codewords[i] = golay_encode(i)
    return codewords

def weight(number):
    number = "{0:024b}".format(number)
    return number.count('1')

# Generates the error patterns for up to 3 errors
def gen_error_patterns():
    error_patterns = np.empty(num_error_patterns, dtype=np.uint32)
    i = 0
    # 2^23 = 0x800000
    for error_pattern in range(0, 0x800000):
      if (weight(error_pattern) <= max_num_errors):
        error_patterns[i] = error_pattern
        i += 1
    return error_patterns

# Calculates the syndrome for a codeword
# Uses the systematic parity check matrix H to do the syndrome calculation by using the formula
# s=rH^T where ^T denotes the transpose of a matrix, r is the received codeword, and 
# s is the syndrome. This is binary matrix multiplication so can simplfy to just taking 
# the index of a bit in the H matrix that is set to 1, and xor'ing all the bits of the 
# codeword that correspond to that index and together looping row by row in the H matrix 
def golay_syndrome(codeword):  
    # systematic H matrix
    H = [[1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,0,0,1,0],
         [0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,0,0,1],
         [0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,1,1,0],
         [0,0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,1,1],
         [0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,1,0,0,0,1,1,1,1],
         [0,0,0,0,0,1,0,0,0,0,0,1,0,0,1,1,1,0,1,0,1,0,1],
         [0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,1,1,1,1,0,0,0],
         [0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,1,1,1,1,0,0],
         [0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,1,1,1,1,0],
         [0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,1,0,1,1,1,1],
         [0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,0,0,1,0,1]]
                                        
    result = ""
    syndrome = 0
    codeword = "{0:023b}".format(codeword)
    for i in range(0, 11):
        syndrome = 0
        for j in range(0, 23):
            if (H[i][j] == 1):
                syndrome ^= int(codeword[j])
        result += str(syndrome)
    return int(result,2)

# Generates the possible syndromes for the case of 3 or 
# fewer errors in the codeword.
def gen_syndromes(error_patterns):
    syndromes = np.empty(num_error_patterns, dtype=np.uint32)
    for i in range(0, num_error_patterns):
        # Using the codeword of 0 to generate the syndromes
        syndromes[i] = golay_syndrome(0^error_patterns[i])
    return syndromes