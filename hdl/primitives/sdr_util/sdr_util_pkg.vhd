-- Utilities Primitive Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules in
-- this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package sdr_util is

  component counter_updown
    generic (
      counter_size_g : integer := 8
      );
    port (
      clk        : in  std_logic;
      reset      : in  std_logic;
      enable     : in  std_logic;
      direction  : in  std_logic;
      load_trig  : in  std_logic;
      load_value : in  unsigned(counter_size_g - 1 downto 0);
      length     : in  unsigned(counter_size_g - 1 downto 0);
      count_out  : out unsigned(counter_size_g - 1 downto 0)
      );
  end component;

  -- For passing opcodes to/from the triggered_sampler primitive
  type triggered_sampler_opcode_t is (
    triggered_sampler_sample_op_e,
    triggered_sampler_time_op_e,
    triggered_sampler_sample_interval_op_e,
    triggered_sampler_flush_op_e,
    triggered_sampler_discontinuity_op_e,
    triggered_sampler_metadata_op_e
    );

  component triggered_sampler
    generic (
      -- This primitive assumes data is one sample per word for the purposes of
      -- maintaining sample time. Therefore data_width_g must be equal to
      -- the sample width where accurate time output messages are important.
      data_width_g           : positive;  -- Width of input and output data.
      capture_length_width_g : positive;  -- Width of capture_length
      little_endian_g        : boolean  -- Defines input/output message
                                        -- word order.
      );
    port (
      -- Control signals
      clk                              : in  std_logic;  -- Single clock.
      rst                              : in  std_logic;  -- Reset.
                                                         -- Active high.
                                                         -- Synch to rising clk.
      -- Properties
      bypass                           : in  std_logic;
      capture_length                   : in  unsigned(capture_length_width_g - 1 downto 0);
      send_flush                       : in  std_logic;
      capture_on_meta                  : in  std_logic;
      capture_single_in                : in  std_logic;
      capture_single_written           : in  std_logic;
      capture_single_out               : out std_logic;
      capture_continuous               : in  std_logic;
      capture_in_progress_out          : out std_logic;
      -- Trigger input stream.
      -- Uses the standard OpenCPI streaming data interface.
      trigger_som                      : in  std_logic;
      trigger_ready                    : in  std_logic;
      trigger_take                     : out std_logic;
      -- Sample data input stream.
      -- Uses the standard OpenCPI streaming data interface.
      input_data                       : in  std_logic_vector(data_width_g - 1 downto 0);
      input_opcode                     : in  triggered_sampler_opcode_t;
      input_som                        : in  std_logic;
      input_eom                        : in  std_logic;
      input_eof                        : in  std_logic;
      input_valid                      : in  std_logic;
      input_ready                      : in  std_logic;
      input_take                       : out std_logic;
      -- Sample data output stream.
      -- Uses the standard OpenCPI streaming data interface.
      output_data                      : out std_logic_vector(data_width_g - 1 downto 0);
      output_opcode                    : out triggered_sampler_opcode_t;
      output_eom                       : out std_logic;
      output_eof                       : out std_logic;
      output_valid                     : out std_logic;
      output_give                      : out std_logic;
      output_ready                     : in  std_logic
      );
  end component;

end package sdr_util;
