-- Pipelined HDL implementation of exponentiation
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Exponentiation calculator
-- Pipelined algorithm to implement exponentiation.
-- input_size_g sets the input data width
-- output_size_g sets the output data width
-- base_g sets the base value of the exponent
-- clk_en clocks in input data on a rising clock edge when high
-- data_valid_in is the data input valid signal
-- data_in is the input data (16-bit fixed point)
-- data_valid_out is the data output valid signal
-- data_out is the result (16-bit fixed point)
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.exp_pkg.all;

entity exponentiation is
  generic (
    input_size_g  : integer := 32;
    output_size_g : integer := 32;
    base_g        : real    := 10.0
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    clk_en         : in  std_logic;
    data_valid_in  : in  std_logic;
    data_in        : in  unsigned(input_size_g - 1 downto 0);
    data_valid_out : out std_logic;
    data_out       : out unsigned(output_size_g - 1 downto 0)
    );
end exponentiation;

architecture rtl of exponentiation is
  -- Set output_width_c so it is a power of two.
  constant log2_output_width_c : integer := integer(ceil(log2(real(
    output_size_g))));
  constant output_width_c : integer := 2**log2_output_width_c;
  -- Initialise exp table
  constant table_c : exp_table_array_t := exp_table_init(base_g,
                                                         output_width_c);
  -- Assign signals and array sizes
  type x_reg_array_t is array (natural range <>) of unsigned(
    output_width_c - 1 downto 0);
  type y_reg_array_t is array (natural range <>) of unsigned(
    output_size_g - 1 downto 0);
  type t_reg_array_t is array (natural range <>) of signed(
    output_width_c - 1 downto 0);
  signal y           : y_reg_array_t(0 to log2_output_width_c * 2 + 23);
  signal x           : x_reg_array_t(0 to log2_output_width_c * 2 + 23);
  signal t           : t_reg_array_t(1 to log2_output_width_c * 2 + 14);
  signal valid_store : std_logic_vector(y'high downto 0);
begin

  -- Delayed valid signal
  valid_store_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        valid_store <= (others => '0');
      elsif clk_en = '1' then
        valid_store <= valid_store(valid_store'high - 1 downto 0) &
                       data_valid_in;
      end if;
    end if;
  end process;

  -- Pipelined exp algorithm
  exp_pipeline_p : process(clk)
    variable i, j : integer;
  begin
    if rising_edge(clk) then
      if rst = '1' then
        x <= (others => (others => '0'));
        y <= (others => (others => '0'));
        t <= (others => (others => '0'));
      elsif clk_en = '1' then
        -- Stage 0
        x(0) <= resize(data_in, x(0)'length);
        y(0) <= to_unsigned(65536, y(0)'length);
        -- Stage 1 to y(y'high)-22
        i    := 0;
        j    := output_width_c / 2;
        while i < log2_output_width_c * 2 loop
          x(i+1) <= x(i);
          y(i+1) <= y(i);
          t(i+1) <= signed(x(i) - to_unsigned(integer(table_c(i/2)),
                                              x(0)'length));
          if t(i+1) >= 0 then
            x(i+2) <= unsigned(t(i+1));
            y(i+2) <= shift_left(y(i+1), j);
          else
            x(i+2) <= x(i+1);
            y(i+2) <= y(i+1);
          end if;
          t(i+2) <= t(i+1);
          i      := i + 2;
          j      := j / 2;
        end loop;
        -- Stage y(y'high)-22 to y(y'high)-8
        j := 1;
        while i < log2_output_width_c * 2 + 14 loop
          x(i+1) <= x(i);
          y(i+1) <= y(i);
          t(i+1) <= signed(x(i) - to_unsigned(integer(table_c(i/2)),
                                              x(0)'length));
          if t(i+1) >= 0 then
            x(i+2) <= unsigned(t(i+1));
            y(i+2) <= y(i+1) + shift_right(y(i+1), j);
          else
            x(i+2) <= x(i+1);
            y(i+2) <= y(i+1);
          end if;
          t(i+2) <= t(i+1);
          i      := i + 2;
          j      := j + 1;
        end loop;
        -- Stage y(y'high)-8 to y(y'high)
        j := 8;
        while i < log2_output_width_c * 2 + 23 loop
          if x(i)(j) = '1' then
            y(i+1) <= y(i) + shift_right(y(i), 16-j);
          else
            y(i+1) <= y(i);
          end if;
          x(i+1) <= x(i);
          i      := i + 1;
          j      := j - 1;
        end loop;
      end if;
    end if;
  end process;

  data_valid_out <= valid_store(y'high);
  data_out       <= y(y'high);

end rtl;
